@echo off

set ESYS_DEV=%~dp0\..\..
set MODBUS_DEV=%~dp0\..\..
set ESYSMODBUS_DEV=%~dp0\..\..
set MYSYSC_DEV=%~dp0\..\..
set ESYS_SYSC=%~dp0\..\..\src\esysc

set ESYSFILE=%~dp0\..\..\src\esysfile
set ESYSMSVC=%~dp0\..\..\src\esysmsvc
set ESYSTEST=%~dp0\..\..\src\esystest
set PYSWIG=%~dp0\..\..\src\pyswig\src\pyswig
set MATPLOTLIB_CPP=%~dp0\..\..\extlib\matplotlib-cpp

set LIBMODBUS=%~dp0\..\..\extlib\libmodbus

"%ESYSSDK_INST_DIR%\bin\esyssdkcli" --call_msvc="2017" "%~dp0\esysmodbus_dev.sln"
